# Ubuntu 18.04 [![build status](https://gitlab.com/nvidia/videosdk/badges/ubuntu18.04/build.svg)](https://gitlab.com/nvidia/videosdk/commits/ubuntu18.04)

## Video Codec SDK 8.2 (with CUDA 9.2)
- `8.2-ubuntu18.04` [(*8.2/Dockerfile*)](https://gitlab.com/nvidia/videosdk/blob/ubuntu18.04/8.2/Dockerfile)
