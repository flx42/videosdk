ARG repository
ARG flavor

FROM ${repository}:9.2-${flavor}-ubuntu18.04

ENV VIDEOSDK_VERSION 8.2.15
LABEL com.nvidia.videosdk.version="${VIDEOSDK_VERSION}"

ENV LIBRARY_PATH /usr/local/cuda/lib64/stubs${LIBRARY_PATH:+:${LIBRARY_PATH}}

ENV NVIDIA_DRIVER_CAPABILITIES ${NVIDIA_DRIVER_CAPABILITIES},video

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        cuda-cudart-dev-$CUDA_PKG_VERSION \
        cuda-driver-dev-$CUDA_PKG_VERSION \
        cuda-misc-headers-$CUDA_PKG_VERSION && \
    apt-get install -y --no-install-recommends unzip curl && \
    VIDEOSDK_DOWNLOAD_SUM=389d5e73b36881b06ca00ea86f0e9c0c312c1646166b96669e8b51324943e213 && \
    curl -fsSL https://developer.download.nvidia.com/compute/redist/VideoCodec/v8.2/NvCodec.zip -O && \
    echo "$VIDEOSDK_DOWNLOAD_SUM  NvCodec.zip" | sha256sum -c - && \
    unzip -j NvCodec.zip \
          NvCodec/NvDecoder/cuviddec.h \
          NvCodec/NvDecoder/nvcuvid.h \
          NvCodec/NvEncoder/nvEncodeAPI.h \
          -d /usr/local/cuda/include && \
    unzip -j NvCodec.zip \
          NvCodec/Lib/linux/stubs/x86_64/libnvcuvid.so \
          NvCodec/Lib/linux/stubs/x86_64/libnvidia-encode.so \
          -d /usr/local/cuda/lib64/stubs && \
    rm NvCodec.zip && \
    apt-get purge --autoremove -y unzip curl && \
    rm -rf /var/lib/apt/lists/*

# pkg-config files
COPY usr /usr

